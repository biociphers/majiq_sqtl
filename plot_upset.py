import argparse
import csv
import matplotlib.pyplot as plt
import pandas as pd
import pyupset as pyu


def get_signif_assocs(fp, args):
    reader = csv.DictReader(fp, dialect='excel-tab')
    yield reader.fieldnames
    yield from filter(lambda line: float(line[args.key]) < args.alpha, reader)


def plot_upset(args):
    plt.figure(figsize=[10, 10])
    outstats = {}
    for name, fp in zip(args.names, args.infiles):
        fieldnames, *assocs = get_signif_assocs(fp, args)
        outstats[name] = pd.DataFrame(assocs, columns=fieldnames)
    pyu.plot(outstats, unique_keys=['lsv_id', 'snp_id'])
    plt.savefig(args.outfile, transparent=True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('outfile',
                        help='Path to output image file')
    parser.add_argument('infiles', nargs='+', type=argparse.FileType(),
                        help='Paths to sQTL call files (TSV), space-separated')
    parser.add_argument('--alpha', type=float, default=0.05,
                        help='P-value or FDR threshold.  Default: %(default)g')
    parser.add_argument('--key', default='linreg_fdr',
                        help='Which column to filter by.  Default: %(default)s')
    parser.add_argument('--names', nargs='+', required=True,
                        help='Name labels to assign to each experiment.  Must be the same length as infiles.')
    args = parser.parse_args()
    assert len(args.names) == len(args.infiles)
    plot_upset(args)


if __name__ == '__main__':
    main()

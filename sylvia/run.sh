#!/bin/sh -xe


## DEFINE VARIABLES
# Path to a python3.6 virtual environment with MAJIQ 2.0 installed
VENV_ROOT=/opt/venv/py3
# Path to the MAJIQ_SQTL clone directory
SRC=${HOME}/majiq_sqtl

## RUN ALL
. ${VENV_ROOT}/bin/activate
sh -xe run_smcec.sh
sh -xe run_gtex.sh
# Upsets
python3 ${SRC}/scripts/upsets_for_paper.py -o upset_by_gene_nominal.pdf -i smc_sqtl.tsv ec_sqtl.tsv aorta_sqtl.tsv coronary_sqtl.tsv tibial_sqtl.tsv -k gene_name -f linreg_pval
python3 ${SRC}/scripts/upsets_for_paper.py -o upset_by_gene_fdr.pdf -i smc_sqtl.tsv ec_sqtl.tsv aorta_sqtl.tsv coronary_sqtl.tsv tibial_sqtl.tsv -k gene_name -f linreg_fdr
python3 ${SRC}/scripts/upsets_for_paper.py -o upset_by_snp_nominal.pdf -i smc_sqtl.tsv ec_sqtl.tsv aorta_sqtl.tsv coronary_sqtl.tsv tibial_sqtl.tsv -k snp_id -f linreg_pval
python3 ${SRC}/scripts/upsets_for_paper.py -o upset_by_snp_fdr.pdf -i smc_sqtl.tsv ec_sqtl.tsv aorta_sqtl.tsv coronary_sqtl.tsv tibial_sqtl.tsv -k snp_id -f linreg_fdr

python3 ${SRC}/scripts/upsets_for_paper.py -o upset_by_gene_nominal.gwas.pdf -i smc_sqtl.gwas.tsv ec_sqtl.gwas.tsv aorta_sqtl.gwas.tsv coronary_sqtl.gwas.tsv tibial_sqtl.gwas.tsv -k gene_name -f linreg_pval
python3 ${SRC}/scripts/upsets_for_paper.py -o upset_by_gene_fdr.gwas.pdf -i smc_sqtl.gwas.tsv ec_sqtl.gwas.tsv aorta_sqtl.gwas.tsv coronary_sqtl.gwas.tsv tibial_sqtl.gwas.tsv -k gene_name -f linreg_fdr
python3 ${SRC}/scripts/upsets_for_paper.py -o upset_by_snp_nominal.gwas.pdf -i smc_sqtl.gwas.tsv ec_sqtl.gwas.tsv aorta_sqtl.gwas.tsv coronary_sqtl.gwas.tsv tibial_sqtl.gwas.tsv -k snp_id -f linreg_pval
python3 ${SRC}/scripts/upsets_for_paper.py -o upset_by_snp_fdr.gwas.pdf -i smc_sqtl.gwas.tsv ec_sqtl.gwas.tsv aorta_sqtl.gwas.tsv coronary_sqtl.gwas.tsv tibial_sqtl.gwas.tsv -k snp_id -f linreg_fdr

# Cleanup
deactivate

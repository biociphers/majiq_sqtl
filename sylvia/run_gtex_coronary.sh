#!/bin/sh -xe

# Paths listed here are for the bone growth project (Penn/CHOP lead: Struan Grant, PhD).
# They refer to files on pickle1.

# Artifacts in the current working directory:
# settings.ini: Preprocessed config for MAJIQ builder
# sqtl.ini: Preprocessed config for sQTL calling
# sqtl.tsv: TSV output from sQTL calling
# sqtl_plots/: Scatterbox and splicegraph plots directory
# sqtl_bedgraphs/: bedGraph.gz files (essentially bigwig) covering the gene locus, delimited by SNP genotype as indicated


## DEFINE VARIABLES
# Path to a python3.6 virtual environment with MAJIQ 2.0 installed
VENV_ROOT=/opt/venv/py3
# Path to the MAJIQ_SQTL clone directory
SRC=${HOME}/majiq_sqtl
# Directory with the BAM files
BAM=/data/gtex/vascular/bam
# Path to the GFF3 annotation
GFF3=/data/DB/hg19/ensembl.hg19.gff3
# Path to experiment VCF file
VCF=/data/gtex/genotype/Coronary.vcf.gz
# Path to covariates file
COVARS=/data/gtex/covariates/Coronary.covariates.txt
# Path to VCF file mapping dbSNP names (GTEx uses generic tags)
DBSNP=/data/gtex/genotype/GRCh37_latest_dbSNP_all.vcf.gz

## RUN ALL
. ${VENV_ROOT}/bin/activate
# sQTL
sed 's/##PSIDIR##/'$(realpath ./psi)'/' coronary_sqtl.ini.in > coronary_sqtl.ini
python3 ${SRC}/call_sqtls.py coronary_sqtl.ini coronary_sqtl.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all
# sQTL with GWAS
python3 ${SRC}/call_sqtls.py coronary_sqtl.ini coronary_sqtl.aaa.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_coronary_ec/metadata/gwas_ld/aaa_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py coronary_sqtl.ini coronary_sqtl.cad.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_coronary_ec/metadata/gwas_ld/cad_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py coronary_sqtl.ini coronary_sqtl.ce.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_coronary_ec/metadata/gwas_ld/ce_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py coronary_sqtl.ini coronary_sqtl.is.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_coronary_ec/metadata/gwas_ld/is_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py coronary_sqtl.ini coronary_sqtl.lvd.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_coronary_ec/metadata/gwas_ld/lvd_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py coronary_sqtl.ini coronary_sqtl.mig.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_coronary_ec/metadata/gwas_ld/mig_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py coronary_sqtl.ini coronary_sqtl.svd.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_coronary_ec/metadata/gwas_ld/svd_gwas_p5e-8_snps_proxy_ld0.8.ld
(head -n1 coronary_sqtl.tsv && tail -n+2 coronary_sqtl.*.tsv) > coronary_sqtl.gwas.tsv
python3 ${SRC}/correct_pvalues.py coronary_sqtl.gwas.tsv
# Plotting
python3 ${SRC}/plot_sqtl.py coronary_sqtl.ini coronary_sqtl.tsv ./coronary_sqtl_plots/ -v ${VCF} --field linreg_fdr --dbsnp ${DBSNP} --normalize off --plot-splicegraph build/splicegraph.sql
# Bedgraphs
python3 ${SRC}/get_sample_beds.py ${VCF} coronary_sqtl.tsv ${GFF3} ./coronary_sqtl_bedgraphs coronary_sqtl.ini ${BAM} --bgzip --column linreg_fdr

# Cleanup
deactivate

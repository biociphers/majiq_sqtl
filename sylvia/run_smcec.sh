#!/bin/sh -xe

# Paths listed here are for the bone growth project (Penn/CHOP lead: Struan Grant, PhD).
# They refer to files on pickle1.

# Artifacts in the current working directory:
# settings.ini: Preprocessed config for MAJIQ builder
# smc.ini: Preprocessed config for sQTL calling
# smc.tsv: TSV output from sQTL calling
# smc_plots/: Scatterbox and splicegraph plots directory
# smc_bedgraphs/: bedGraph.gz files (essentially bigwig) covering the gene locus, delimited by SNP genotype as indicated
# ec.ini: Preprocessed config for sQTL calling
# ec.tsv: TSV output from sQTL calling
# ec_plots/: Scatterbox and splicegraph plots directory
# ec_bedgraphs/: bedGraph.gz files (essentially bigwig) covering the gene locus, delimited by SNP genotype as indicated


## DEFINE VARIABLES
# Path to a python3.6 virtual environment with MAJIQ 2.0 installed
VENV_ROOT=/opt/venv/py3
# Path to the MAJIQ_SQTL clone directory
SRC=${HOME}/majiq_sqtl
# Directory with the BAM files
BAM=/data/sylvia_smc_ec/bam
# Path to the GFF3 annotation
GFF3=/data/DB/hg19/ensembl.hg19.gff3
# Path to experiment VCF file
VCF=/data/sylvia_smc_ec/genotype/SMCEM.sanger.filtered.vcf.gz
# Path to covariates file
COVARS=/data/gtex/metadata/Covariates.txt
# Path to VCF file mapping dbSNP names (GTEx uses generic tags)
DBSNP=/data/gtex/genotype/GRCh37_latest_dbSNP_all.vcf.gz

## RUN ALL
. ${VENV_ROOT}/bin/activate
# Ensure the environment is up to date
pip install -U pip
pip install -U -r ${SRC}/requirements.txt
# Builder
sed 's/##BAMDIR##/'${BAM}'/' settings.ini.in > settings.ini
majiq build ${GFF3} -o build -c settings.ini -j 16
# Psi
for majiqFile in build/*.majiq; do
    majiq psi ${majiqFile} -o psi -j 16
done
# Voila
for voilaFile in psi/*.voila; do
    voila tsv -f ${voilaFile%.*}.tsv ${voilaFile} build
done
# sQTL, SMC
sed 's/##PSIDIR##/'$(realpath ./psi)'/' smc_sqtl.ini.in > smc_sqtl.ini
python3 ${SRC}/call_sqtls.py smc_sqtl.ini smc_sqtl.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all
# sQTL, SMC, with GWAS
python3 ${SRC}/call_sqtls.py smc_sqtl.ini smc_sqtl.aaa.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_smc_ec/metadata/gwas_ld/aaa_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py smc_sqtl.ini smc_sqtl.cad.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_smc_ec/metadata/gwas_ld/cad_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py smc_sqtl.ini smc_sqtl.ce.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_smc_ec/metadata/gwas_ld/ce_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py smc_sqtl.ini smc_sqtl.is.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_smc_ec/metadata/gwas_ld/is_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py smc_sqtl.ini smc_sqtl.lvd.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_smc_ec/metadata/gwas_ld/lvd_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py smc_sqtl.ini smc_sqtl.mig.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_smc_ec/metadata/gwas_ld/mig_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py smc_sqtl.ini smc_sqtl.svd.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_smc_ec/metadata/gwas_ld/svd_gwas_p5e-8_snps_proxy_ld0.8.ld
(head -n1 smc_sqtl.tsv && tail -n+2 smc_sqtl.*.tsv) > smc_sqtl.gwas.tsv
python3 ${SRC}/correct_pvalues.py smc_sqtl.gwas.tsv
# Plotting, SMC
python3 ${SRC}/plot_sqtl.py smc_sqtl.ini smc_sqtl.tsv ./smc_sqtl_plots/ -v ${VCF} --field linreg_fdr --dbsnp ${DBSNP} --normalize off --plot-splicegraph build/splicegraph.sql
# Bedgraphs, SMC
python3 ${SRC}/get_sample_beds.py ${VCF} smc_sqtl.tsv ${GFF3} ./smc_sqtl_bedgraphs smc_sqtl.ini ${BAM} --bgzip --column linreg_fdr
# sQTL, EC
sed 's/##PSIDIR##/'$(realpath ./psi)'/' ec_sqtl.ini.in > ec_sqtl.ini
python3 ${SRC}/call_sqtls.py ec_sqtl.ini ec_sqtl.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all
# sQTL, EC, with GWAS
python3 ${SRC}/call_sqtls.py ec_sqtl.ini ec_sqtl.aaa.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_ec_ec/metadata/gwas_ld/aaa_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py ec_sqtl.ini ec_sqtl.cad.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_ec_ec/metadata/gwas_ld/cad_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py ec_sqtl.ini ec_sqtl.ce.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_ec_ec/metadata/gwas_ld/ce_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py ec_sqtl.ini ec_sqtl.is.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_ec_ec/metadata/gwas_ld/is_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py ec_sqtl.ini ec_sqtl.lvd.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_ec_ec/metadata/gwas_ld/lvd_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py ec_sqtl.ini ec_sqtl.mig.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_ec_ec/metadata/gwas_ld/mig_gwas_p5e-8_snps_proxy_ld0.8.ld
python3 ${SRC}/call_sqtls.py ec_sqtl.ini ec_sqtl.svd.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all --gwas /data/sylvia_ec_ec/metadata/gwas_ld/svd_gwas_p5e-8_snps_proxy_ld0.8.ld
(head -n1 ec_sqtl.tsv && tail -n+2 ec_sqtl.*.tsv) > ec_sqtl.gwas.tsv
python3 ${SRC}/correct_pvalues.py ec_sqtl.gwas.tsv
# Plotting, EC
python3 ${SRC}/plot_sqtl.py ec_sqtl.ini ec_sqtl.tsv ./ec_sqtl_plots/ -v ${VCF} --field linreg_fdr --dbsnp ${DBSNP} --normalize off --plot-splicegraph build/splicegraph.sql
# Bedgraphs, EC
python3 ${SRC}/get_sample_beds.py ${VCF} ec_sqtl.tsv ${GFF3} ./ec_sqtl_bedgraphs ec_sqtl.ini ${BAM} --bgzip --column linreg_fdr

# Cleanup
deactivate

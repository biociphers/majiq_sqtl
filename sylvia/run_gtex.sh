#!/bin/sh -xe

# Paths listed here are for the bone growth project (Penn/CHOP lead: Struan Grant, PhD).
# They refer to files on pickle1.

# Artifacts in the current working directory:
# settings.ini: Preprocessed config for MAJIQ builder
# sqtl.ini: Preprocessed config for sQTL calling
# sqtl.tsv: TSV output from sQTL calling
# sqtl_plots/: Scatterbox and splicegraph plots directory
# sqtl_bedgraphs/: bedGraph.gz files (essentially bigwig) covering the gene locus, delimited by SNP genotype as indicated


## DEFINE VARIABLES
# Path to a python3.6 virtual environment with MAJIQ 2.0 installed
VENV_ROOT=/opt/venv/py3
# Path to the MAJIQ_SQTL clone directory
SRC=${HOME}/majiq_sqtl
# Directory with the BAM files
BAM=/data/gtex/vascular/bam
# Path to the GFF3 annotation
GFF3=/data/DB/hg19/ensembl.hg19.gff3

## RUN ALL
. ${VENV_ROOT}/bin/activate
# Ensure the environment is up to date
pip install -U pip
pip install -U -r ${SRC}/requirements.txt
# Builder
sed 's/##BAMDIR##/'${BAM}'/' settings.ini.in > settings.ini
majiq build ${GFF3} -o build -c settings.ini -j 16
# Psi
for majiqFile in build/*.majiq; do
    majiq psi ${majiqFile} -o psi -j 16
done
# Voila
for voilaFile in psi/*.voila; do
    voila tsv -f ${voilaFile%.*}.tsv ${voilaFile} build
done
# sQTLs
sh -xe run_gtex_aorta.sh
sh -xe run_gtex_coronary.sh
sh -xe run_gtex_tibial.sh
# Cleanup
deactivate

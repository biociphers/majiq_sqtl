import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as pch
import argparse
import pysam
import numpy as np
from voila.api import SpliceGraphs
import os
import sys
import scipy.stats as st
from progressbar import ProgressBar
from src.voila_io import load_lsvs
from src.utils import common
# from adjustText import adjust_text

MAX_PLOTS_PER_ROW = 5
SCALAR = 100.0
bar = ProgressBar()


def load_genes(splicegraphs, lsvs):
    genes = {}
    bar.start(len(lsvs))
    for lsv in bar(lsvs.values()):
        if not genes.has_key(lsv['gene_id']):
            try:
                gene = splicegraphs.get_gene(lsv['gene_id'])
            except KeyError:
                continue
            gene_dict = gene.__dict__.copy()
            gene_dict.update(exons=[], junctions=[])
            for exon in gene.exons:
                gene_dict['exons'].append(exon.to_dict())
            for junction in gene.junctions:
                gene_dict['junctions'].append(junction.to_dict())
            gene_dict['start'] = gene.start()
            gene_dict['end'] = gene.end()
            genes[gene.gene_id] = gene_dict
    return genes


def load_snps(vcf, snp_ids, mapping=None):
    snps = {}
    if mapping is None:
        for snp in vcf.fetch():
            if snp.id in snp_ids:
                snps[snp.id] = snp
    else:
        for snp in vcf.fetch():
            if snp.id in mapping['vcf']:
                snp_id = mapping['sqtl'][mapping['vcf'] == snp.id][0]
                snp.id = snp_id.tobytes()
            if snp.id in snp_ids:
                snps[snp.id] = snp
    return snps


def plot_lsv_graph(lsv, gene, prefix, extn='.pdf', snps=None, vcf=None):
    matplotlib.rc('font', size=16)
    width_a = abs(float(gene['end'] - gene['start']))
    factor = min(SCALAR / width_a, 1.0)
    height = abs(width_a / 10)
    plt.figure(figsize=(20, 2))
    axes = plt.gca()
    axes.yaxis.set_visible(False)
    exon_rects = []
    lsv_coords = tuple(sorted((lsv['start'], lsv['end'])))
    seen_refexon = False
    for exon in gene['exons']:
        exon_coords = tuple(sorted((exon['start'], exon['end'])))
        if exon_coords == lsv_coords:
            facecolor = (254. / 255, 228. / 255, 191. / 255)
            zorder = 5
            seen_refexon = True
        else:
            facecolor = (217. / 255, 217. / 255, 217. / 255)
            zorder = 3
        if exon['intron_retention']:
            y = height * 0.2
            ht = height * 0.1
        else:
            y = 0
            ht = height * 0.5
        exon_rects.append(pch.Rectangle((exon['start'] * factor, y * factor),
                                        (exon['end'] - exon['start']) *
                                        factor, ht * factor,
                                        facecolor=facecolor,
                                        edgecolor=(0, 0, 0), linewidth=2,
                                        zorder=zorder))
    if not seen_refexon:
        exon_rects.append(pch.Rectangle((lsv_coords[0] * factor, 0),
                                        (lsv_coords[1] - lsv_coords[0]) *
                                        factor, 0.5 * height * factor,
                                        facecolor=(254. / 255,
                                                   228. / 255,
                                                   191. / 255),
                                        edgecolor=(0, 0, 0), linewidth=4,
                                        zorder=5))
    junc_arcs = []
    lsv.cycler = axes._get_lines.prop_cycler
    junctions = [tuple(sorted(junction)) for junction in lsv.junctions]
    leftmost, rightmost = lsv_coords
    if len(junctions) == 1 or junctions[0][1] == junctions[1][1]:
        vlineend = 0
    else:
        vlineend = 1
    for junction in gene['junctions']:
        junction_coords = tuple(sorted((junction['start'], junction['end'])))
        width = abs(float(junction['end'] - junction['start']))
        if junction_coords in junctions:
            jid = junctions.index(junction_coords)
            edgecolor = lsv.color(jid)
            linestyle = 'solid'
            linewidth = 2
            zorder = 2
            leftmost = min(leftmost, junction_coords[0])
            rightmost = max(rightmost, junction_coords[1])
        else:
            edgecolor = (217. / 255, 217. / 255, 217. / 255)
            linestyle = 'dotted'
            linewidth = 1
            zorder = 1
        if junction['intron_retention'] and junction_coords in junctions:
            plt_coords = (junction_coords[0] * factor,
                          junction_coords[1] * factor)
            for patch in exon_rects:
                x, _ = patch.xy
                r = x + patch.get_width()
                if plt_coords == (x, r):
                    patch.set_color(edgecolor)
                    break
            else:
                exon_rects.append(pch.Rectangle((plt_coords[0],
                                                 height * 0.2 * factor),
                                                plt_coords[1] - plt_coords[0],
                                                height * 0.1 * factor,
                                                facecolor=edgecolor,
                                                edgecolor=(0, 0, 0),
                                                linewidth=2, zorder=6))
        elif not junction['intron_retention']:
            x = np.mean(junction_coords)
            ht = min(abs(height), abs(width))
            junc_arcs.append(pch.Arc((x * factor, height / 2 * factor),
                                     width * factor, ht * factor,
                                     angle=0, theta1=0, theta2=180,
                                     edgecolor=edgecolor, linewidth=linewidth,
                                     linestyle=linestyle, zorder=zorder))
            if junction_coords in junctions:
                jid = junctions.index(junction_coords)
                jcrd = junction_coords[vlineend]
                plt.plot((jcrd * factor, jcrd * factor),
                         (0, 0.5 * height * factor), color=edgecolor,
                         linestyle='dotted', zorder=6, label='#%d' % jid)
    if vcf is not None:
        none_found = True
        snp_lines = []
        snp_texts = []
        if snps is not None:
            red_snps = [snp.id for snp in snps]
        else:
            red_snps = []
        for snp in vcf.fetch(contig=gene['chromosome'], start=leftmost,
                             end=rightmost):
            color = (255, 0, 0) if snp.id in red_snps else (0, 0, 0)
            snp_lines.append(plt.plot((snp.pos * factor, snp.pos * factor),
                                      (0.175 * height * factor,
                                       0.325 * height * factor),
                                      linewidth=4, color=color,
                                      zorder=8)[0].get_xdata()[0])
            snp_texts.append(plt.text(snp.pos * factor,
                                      -height * 0.3 * factor, snp.id,
                                      zorder=8,
                                      horizontalalignment='center'))
        if none_found:
            plt.close()
            return None
        # adjust_text(snp_texts, ax=axes, add_objects=exon_rects)
    for patch in exon_rects + junc_arcs:
        axes.add_patch(patch)
    width_b = rightmost - leftmost
    xmin = (leftmost - width_b * 1.5) * factor
    xmax = (rightmost + width_b * 1.5) * factor
    axes.set_xlim(xmin, xmax)
    tkwd = 10 ** np.ceil(np.log10(width_b * 4.0) - 1)
    if tkwd / (width_b * 4) > 1. / 2:
        tkwd /= 2
    xticks = np.arange(np.ceil((leftmost - width_b * .5) / tkwd) * tkwd,
                       np.ceil((rightmost + width_b * .5) / tkwd) * tkwd,
                       tkwd).astype(int)
    axes.set_xticks(xticks * factor)
    xticklabels = map('{:d}'.format, xticks.astype(int))
    axes.set_xticklabels(xticklabels)
    axes.set_ylim(-height * 0.4 * factor, height * 1.2 * factor)
    axes.set_xlabel('%s at %s:%d-%d' % (gene['name'], gene['chromosome'],
                                        lsv.start, lsv.end))
    leg = axes.legend(loc='right')
    leg.get_frame().set_alpha(1.0)
    plt.tight_layout(0.05)
    if snps is None:
        fname = '%s_%s%s' % (prefix, lsv.id, extn)
    else:
        fname = '%s_snps_%s_sgph%s' % (prefix, lsv.id, extn)
    plt.savefig(fname, transparent=True)
    plt.close()
    return fname


def plot_psi_scatter(lsv, snp, stats, prefix, which_stat='kruskal',
                     extn='.pdf'):
    matplotlib.rc('font', size=24)
    alleles = snp.alleles
    njunc = lsv.njunc
    hom_left = '%s/%s' % (alleles[0], alleles[0])
    hetero = '%s/%s' % alleles
    hom_right = '%s/%s' % (alleles[1], alleles[1])
    psi = ([], [], [])
    samples = ([], [], [])
    for name, samp in snp.samples.items():
        if name in lsv.samples and samp.allele_indices != (None,):
            gnt = sum(samp.allele_indices)
            samples[gnt].append(name)
            psi[gnt].append(lsv.samples[name])
    for ct in range(3):
        mu = np.mean(psi[ct], axis=0)
        devs = [np.abs(psi_s - mu).sum() for psi_s in psi[ct]]
        mindev = np.argmin(devs)
        print samples[ct][mindev]
    psi = map(np.transpose, psi)
    cts = np.array([jn.shape[1] for jn in psi])
    widths = cts.astype(float) / cts.sum()
    lsv.cycler = plt.gca()._get_lines.prop_cycler
    signif_jns = []
    for jn in range(njunc):
        mdiff = np.median(psi[2][jn]) - np.median(psi[0][jn])
        if abs(mdiff) < 0.05:
            continue
        cur_psi = [psi[ct][jn] for ct in range(3)]
        fstat, fpval = st.f_oneway(*cur_psi)
        if not np.isnan(fpval) and fpval < 0.05:
            signif_jns.append(jn)
        # mdiff = np.median(psi[2][jn]) - np.median(psi[0][jn])
        # if abs(mdiff) >= 0.05:
        #     signif_jns.append(jn)
    actual_njunc = len(signif_jns)
    if actual_njunc == 0:
        return
    n_across = min(MAX_PLOTS_PER_ROW, actual_njunc)
    n_down = np.ceil(actual_njunc * 1.0 / n_across)
    plt.figure(figsize=(max(20, 10 * n_across), 10 * n_down))
    for sjid, jn in enumerate(signif_jns):
        edgecolor = lsv.color(jn)
        ax = plt.subplot(n_down, n_across, sjid + 1)
        # ax.set_aspect('equal', anchor='C')
        for ct in range(3):
            if cts[ct]:
                jitter = (np.random.rand(cts[ct]) - 0.5) * widths[ct]
                ax.boxplot(psi[ct][jn], positions=(ct + 1,),
                           zorder=2)
                ax.scatter(jitter + ct + 1, psi[ct][jn], color=edgecolor,
                           zorder=1)
        mdiff = np.median(psi[2][jn]) - np.median(psi[0][jn])
        ax.set_xlim(0.5, 3.5)
        ax.set_ylim(-0.05, 1.05)
        ax.set_xticks(range(1, 4))
        ax.set_xticklabels(('{:s} (N = {:d})'.format(hom_left, cts[0]),
                            '{:s} (N = {:d})'.format(hetero, cts[1]),
                            '{:s} (N = {:d})'.format(hom_right, cts[2])))
        ax.set_xlabel('Genotype class (Number of samples)\n'
                      '$\\hat{{\\Delta\\Psi}} = {:0.03f}$'.format(mdiff))
        ax.set_ylabel('$\\Psi$ of junction #{:d}'.format(jn))
    pname = '%s_pval' % which_stat
    fdrname = '%s_fdr' % which_stat
    distance = 1000000
    for exon in lsv.exons + lsv.junctions:
        distance = min(abs(exon[0] - snp.pos),
                       abs(snp.pos - exon[1]),
                       distance)
    plt.suptitle('%s vs %s\n'
                 'p = %.3g (fdr = %.3g), %d bp from nearest SJ or exon '
                 'boundary'
                 % (lsv.id, stats['snp_id'], stats[pname],
                    stats[fdrname], distance))
    plt.tight_layout(0.8, rect=(0, 0, 1, .9))
    fname = '%s_%s_%s%s' % (prefix, lsv.id, stats['snp_id'], extn)
    plt.savefig(fname, transparent=True)
    plt.close()
    return fname


def load_association(fname):
    return np.genfromtxt(fname, dtype=None, names=True, delimiter='\t')


def load_mapping(fname):
    return np.genfromtxt(fname, dtype=None, names=('vcf', 'sqtl'),
                         delimiter='\t')


def main():
    parser = argparse.ArgumentParser(parents=[common])
    parser.add_argument('--vcf', type=pysam.VariantFile, required=True)
    parser.add_argument('--names', nargs='+', required=True)
    parser.add_argument('--assocfile', dest='assoc', type=load_association,
                        required=True)
    parser.add_argument('--splice-graphs', type=SpliceGraphs, required=True)
    parser.add_argument('--output', dest='prefix', required=True)
    parser.add_argument('--extn', default='.pdf')
    parser.add_argument('--stat', default='kruskal')
    parser.add_argument('--fdr', type=float, default=0.05)
    parser.add_argument('--replace-snp-ids', dest='mapping', type=load_mapping)
    args = parser.parse_args()
    try:
        os.makedirs(os.path.split(args.prefix)[0])
    except OSError:
        pass
    lsvs = load_lsvs(args)
    snp_ids = np.unique(args.assoc['snp_id']).tolist()
    snps = load_snps(args.vcf, snp_ids, mapping=args.mapping)
    sys.stderr.write('Plotting...\n')
    sys.stderr.flush()
    fdrname = '%s_fdr' % args.stat
    seen_exons = []
    fnames = []
    significant_rows = args.assoc[args.assoc[fdrname] < args.fdr]
    genes = {}
    with SpliceGraphs(args.splice_graphs, 'r') as sg:
        for lsv_id, lsv in lsvs.items():
            lsv_rows = significant_rows[significant_rows['lsv_id'] == lsv_id]
            if 'colors' not in lsv:
                lsv['colors'] = {}
            cur_snps = []
            for row in lsv_rows:
                snp = snps.get(row['snp_id'])
                if snp is None:
                    if row['snp_id'] in args.mapping['sqtl']:
                        idx, = np.where(args.mapping['sqtl'] == row['snp_id'])[0]
                        snp_id = args.mapping['vcf'][idx]
                    elif row['snp_id'] in args.mapping['vcf']:
                        idx, = np.where(args.mapping['vcf'] == row['snp_id'])[0]
                        snp_id = args.mapping['sqtl'][idx]
                    else:
                        raise ValueError('unrecognized SNP ID: %s', row['snp_id'])
                    snp = snps[snp_id]
                cur_snps.append(snp)
            gene = genes.get(lsvs['gene_id'])
            if gene is None:
                try:
                    rgene = sg.get_gene(lsv.gene_id)
                except KeyError:
                    continue
                gene = rgene.__dict__.copy()
                gene.update(exons=[], junctions=[])
                for exon in rgene.exons:
                    gene['exons'].append(exon.to_dict())
                for junction in rgene.junctions:
                    gene['junctions'].append(junction.to_dict())
                gene['start'] = rgene.start()
                gene['end'] = rgene.end()
                genes[rgene.gene_id] = gene

            # LSV redundancy filter
            exons = [(exon['start'], exon['end']) for exon in lsv['exons']]
            ref_exon = (lsv.start, lsv.end)
            if ref_exon in exons:
                exons.remove(ref_exon)
            if any([exon in seen_exons for exon in exons]):
                for exon in exons:
                    if exon not in seen_exons:
                        seen_exons.append(exon)
            else:
                seen_exons += exons

            # Inferred perfect LD filter
            fnames.append(plot_lsv_graph(lsv, gene, args.prefix, extn=args.extn,
                                         snps=cur_snps, vcf=args.vcf))

            # Actually plot it
            if fnames[-1] is not None:
                for row, snp in zip(lsv_rows, cur_snps):
                    region = '%s:%d-%d' % (gene['chromosome'],
                                           gene['start'],
                                           gene['end'])
                    print region, lsv.id, snp.id
                    fnames.append(plot_psi_scatter(lsv, snp, row, args.prefix,
                                                   extn=args.extn,
                                                   which_stat=args.stat))
        # for fname in fnames:
        #     if fname is not None:
        #         print fname


if __name__ == '__main__':
    main()

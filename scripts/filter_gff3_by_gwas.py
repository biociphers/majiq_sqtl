import argparse
import csv
from collections import defaultdict
from filter_gff3_common import Gff3


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('ingff3', type=Gff3)
    parser.add_argument('outgff3', type=argparse.FileType('w'))
    parser.add_argument('gwas_ld', type=argparse.FileType(), nargs='+')
    parser.add_argument('--distance', type=int, default=500)
    args = parser.parse_args()

    args.ingff3.read()

    gene_ids = set()
    gwas_rows = defaultdict(set)
    for gwas in args.gwas_ld:
        for row in csv.DictReader(gwas,
                                  delimiter=' ',
                                  skipinitialspace=True,
                                  lineterminator='\n'):
            if not row['CHR_A'].startswith('chr'):
                row['CHR_A'] = 'chr' + row['CHR_A']
            if not row['CHR_B'].startswith('chr'):
                row['CHR_B'] = 'chr' + row['CHR_B']
            gwas_rows[row['CHR_A']].add(int(row['BP_A']))
            gwas_rows[row['CHR_B']].add(int(row['BP_B']))
    for chrom, gffrows in args.ingff3.data.items():
        print('Assessing variants in', chrom)
        for gffrow in gffrows:
            if gffrow.feature != 'gene':
                continue
            for snppos in gwas_rows[chrom]:
                if gffrow.start - args.distance <= snppos <= gffrow.end + args.distance:
                    gene_ids.add(gffrow.ID)
                    break
    print(gene_ids)
    if len(gene_ids) > 0:
        args.ingff3.apply_filter(gene_ids)
        args.ingff3.write(args.outgff3)
    else:
        raise ValueError('got no genes')


if __name__ == '__main__':
    main()

import argparse
import pysam
import numpy as np
import allel


def get_snp_genotypes(snp, sampnames):
    output = []
    for name in sampnames:
        try:
            output.append(sum(snp.samples[str(name)].allele_indices))
        except Exception as e:
            output.append(np.nan)
    return output


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('vcf', type=pysam.VariantFile)
    parser.add_argument('out', type=argparse.FileType('w'))
    args = parser.parse_args()

    sampnames = list(args.vcf.header.samples)
    gnt = np.array([get_snp_genotypes(snp, sampnames)
                    for snp in args.vcf.fetch()], dtype=np.int8)
    loc = allel.locate_unlinked(gnt)
    pcs, model = allel.pca(gnt[loc], n_components=3)
    args.out.write('ID\t' + '\t'.join(sampnames) + '\n')
    fmt = '{:.4f}'.format
    for i in range(3):
        args.out.write(('PC%d\t' % i) + '\t'.join(map(fmt, pcs[:, i])) + '\n')


if __name__ == '__main__':
    main()

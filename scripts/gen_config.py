import argparse
import configparser


class MajiqConfig(configparser.ConfigParser):
    """A ConfigParser, but case sensitive"""
    optionxform = str


def main():
    """Main routine"""
    parser = argparse.ArgumentParser()
    parser.add_argument('tissue', choices=('HCASMC', 'HCAEC'))
    parser.add_argument('source')
    parser.add_argument('psidir')
    parser.add_argument('output', type=argparse.FileType('w'))
    parser.add_argument('--extension', choices=('voila', 'tsv'), default='tsv')
    args = parser.parse_args()

    config = MajiqConfig()
    config.read(args.source)
    outconf = MajiqConfig()
    outconf.add_section('info')
    outconf.set('info', 'psidir', args.psidir)
    outconf.add_section('psifiles')
    for key, value in config.items('psifiles'):
        outconf.set('psifiles', key, value.format(tissue=args.tissue, extn=args.extension))
    outconf.add_section('contigs')
    for key, value in config.items('contigs'):
        outconf.set('contigs', key, value)
    outconf.write(args.output)


if __name__ == '__main__':
    main()

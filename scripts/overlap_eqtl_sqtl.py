import argparse
import csv
import pysam


refseq2ncbiChromosomes = {
    'chr1': 'NC_000001.10',
    'chr2': 'NC_000002.11',
    'chr3': 'NC_000003.11',
    'chr4': 'NC_000004.11',
    'chr5': 'NC_000005.9',
    'chr6': 'NC_000006.11',
    'chr7': 'NC_000007.13',
    'chr8': 'NC_000008.10',
    'chr9': 'NC_000009.11',
    'chr10': 'NC_000010.10',
    'chr11': 'NC_000011.9',
    'chr12': 'NC_000012.11',
    'chr13': 'NC_000013.10',
    'chr14': 'NC_000014.8',
    'chr15': 'NC_000015.9',
    'chr16': 'NC_000016.9',
    'chr17': 'NC_000017.10',
    'chr18': 'NC_000018.9',
    'chr19': 'NC_000019.9',
    'chr20': 'NC_000020.10',
    'chr21': 'NC_000021.8',
    'chr22': 'NC_000022.10',
    'chrX': 'NC_000023.10',
    'chrY': 'NC_000024.9'
}


def overlap_eqtl_sqtl(args):
    print('reading eqtls')
    eqtls = list(csv.DictReader(args.eqtls, dialect='excel-tab'))
    print('reading sqtls')
    sqtls = list(csv.DictReader(args.sqtls, dialect='excel-tab'))
    print('matching')
    output = []
    for row in sqtls:
        if float(row['linreg_fdr']) < 0.05:
            gene_id = row['lsv_id'].split(':')[0]
            for erow in eqtls:
                if erow['gene'].split('.')[0] != gene_id:
                    continue
                if float(erow['p-value']) >= 0.05:
                    continue
                snp_pos = int(erow['SNP'].split('_')[1])
                contig = row['# chromosome']
                if ':' in contig:
                    contig = contig.split(':')[1]
                contig = refseq2ncbiChromosomes[contig]
                for snp in args.dbsnp.fetch(contig, snp_pos - 1, snp_pos + 1):
                    if snp.pos == snp_pos and snp.id == row['snp_id']:
                        break
                else:
                    continue
                output.append({
                    'gene_name': row['gene_name'],
                    'lsv_id': row['lsv_id'],
                    'snp_id': snp.id,
                    'sqtl_beta': row['slope'],
                    'sqtl_pval': row['linreg_pval'],
                    'sqtl_fdr': row['linreg_fdr'],
                    'eqtl_beta': erow['beta'],
                    'eqtl_pval': erow['p-value']
                })
    print('writing')
    fieldnames = list(output[0].keys())
    writer = csv.DictWriter(args.output, fieldnames, dialect='excel-tab')
    writer.writeheader()
    writer.writerows(output)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('eqtls', type=argparse.FileType())
    parser.add_argument('sqtls', type=argparse.FileType())
    parser.add_argument('output', type=argparse.FileType('w'))
    parser.add_argument('--dbsnp', type=pysam.VariantFile)
    args = parser.parse_args()
    overlap_eqtl_sqtl(args)


if __name__ == '__main__':
    main()

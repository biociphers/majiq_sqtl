import matplotlib.pyplot as plt
import sys
import numpy as np

assert len(sys.argv) == 4

arr = np.genfromtxt(sys.argv[1], encoding=None, delimiter='\t', names=True,
                    dtype=None)
plt.hist(arr[sys.argv[2]], bins=np.linspace(0, 1, num=21), normed=True, rwidth=0.9)
plt.xlabel('P value')
plt.ylabel('Relative frequency')
plt.savefig(sys.argv[3], transparent=True)

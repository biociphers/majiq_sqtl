import ConfigParser as configparser
import argparse
import csv
import traceback


def filter_list(sample, rows, fieldnames):
    print '%d results found for %s' % (len(rows), sample)
    print 'Fields:'
    for field in fieldnames:
        print '\t' + field
    field = raw_input('Select field to filter on: ')
    values = [row[field] for row in rows]
    unique = list(set(values))
    if len(unique) > 1:
        print 'Please choose one of these values'
        for i, value in enumerate(unique):
            print '%d: %s (%d)' % (i + 1, value, values.count(value))
        choice = int(raw_input('Please enter your selection: ')) - 1
        assert choice in range(len(unique))
        rows = [row for row in rows if row[field] == unique[choice]]
    return rows


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('sra_table', type=argparse.FileType())
    parser.add_argument('config', type=argparse.FileType('w'))
    parser.add_argument('tissue')
    parser.add_argument('psidir')
    args = parser.parse_args()

    reader = csv.DictReader(args.sra_table, dialect='excel-tab')
    config = configparser.ConfigParser()
    config.optionxform = str
    config.add_section('info')
    config.set('info', 'psidir', args.psidir)
    config.add_section('psifiles')

    samples = {}
    for row in reader:
        if row['body_site_s'] == args.tissue:
            samples.setdefault(row['submitted_subject_id_s'], [])
            samples[row['submitted_subject_id_s']].append(row)
    if len(samples) == 0:
        raise ValueError('no runs for tissue "%s"' % args.tissue)

    for sample, rows in samples.items():
        while len(rows) > 1:
            try:
                rows = filter_list(sample, rows, reader.fieldnames)
            except KeyboardInterrupt:
                print ''
                print 'Interrupted'
                exit()
            except Exception as e:
                print 'Invalid input (exact error is as follows)'
                print traceback.format_exc()
        config.set('psifiles', sample, '%s.psi.voila' % rows[0]['Run_s'])

    config.write(args.config)


if __name__ == '__main__':
    main()

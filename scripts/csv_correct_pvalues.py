import argparse
import csv
import statsmodels.stats.multitest as mt
import numpy as np


def csv_correct_pvalues(args):
    with open(args.filepath) as fp:
        reader = csv.DictReader(fp, dialect='excel-tab')
        data = list(reader)
    out_fieldnames = [x for x in reader.fieldnames]
    for fieldname in args.fieldname:
        if fieldname in reader.fieldnames:
            if args.correct_by is not None:
                seen_genes = {}
                all_pvalues = []
                for row in data:
                    pval = float(row[fieldname])
                    seen_genes[row[args.correct_by]] = min(seen_genes.get(row[args.correct_by], 1.0), pval)
                    all_pvalues.append(pval)
                pvalues = list(seen_genes.values())
                print(sorted(pvalues))
                rejected, corrected_by, _, _ = mt.multipletests(pvalues, alpha=args.alpha, method='fdr_bh', is_sorted=False)
                corrected = np.interp(all_pvalues, pvalues, corrected_by, left=0.0, right=1.0)
            else:
                pvalues = [float(row[fieldname]) for row in data]
                rejected, corrected, _, _ = mt.multipletests(pvalues, alpha=args.alpha, method='fdr_bh', is_sorted=False)
            out_fieldnames.append(f'{fieldname}_fdr')
            for row, fdr in zip(data, corrected):
                row[f'{fieldname}_fdr'] = f'{fdr:g}'
    with open(args.outfile, 'w') as ofp:
        writer = csv.DictWriter(ofp, out_fieldnames, dialect='excel-tab')
        writer.writeheader()
        writer.writerows(data)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('filepath')
    parser.add_argument('outfile')
    parser.add_argument('-f', '--fieldname', nargs='+')
    parser.add_argument('-a', '--alpha', type=float, default=0.05)
    parser.add_argument('-c', '--correct-by')
    args = parser.parse_args()
    csv_correct_pvalues(args)


if __name__ == '__main__':
    main()

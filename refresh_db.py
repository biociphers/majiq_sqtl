import argparse
from src.voila_io import load_lsvs
from src.utils import common

parser = argparse.ArgumentParser(parents=[common])
args = parser.parse_args()
assert args.pre_lsv is not None
args.refresh_db = True
load_lsvs(args)

#!/bin/python


import argparse
import glob
import numpy as np
import os
import pybedtools
import pysam
from src.utils import load_config, gtex_convert_name


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('vcffile',
                        help='Path to BGZIPed and indexed variant call file')
    parser.add_argument('assocfile',
                        help='Path to TSV output of call_sqtls.py')
    parser.add_argument('gff3',
                        help='Path to GFF3 transcriptome annotation, should be the same passed to the MAJIQ builder')
    parser.add_argument('outdir',
                        type=lambda d: os.makedirs(d, exist_ok=True) or d,
                        help='Output directory.  Will be created if does not already exist.')
    parser.add_argument('config', type=load_config,
                        help='Configuration file which defines the sample ID mapping for sQTL')
    parser.add_argument('bams', type=lambda path: glob.glob(os.path.join(path, '*.bam')),
                        help='Path to the directory containing the BAM files')
    parser.add_argument('--column', default='linreg_fdr',
                        help='Which TSV column to filter on.  Default: %(default)s')
    parser.add_argument('--threshold', type=float, default=0.05,
                        help='P-value or FDR threshold.  Default: %(default)g')
    parser.add_argument('--bgzip', action='store_true',
                        help='BGZIP-compress the output bedgraphs')
    parser.add_argument('--keep-intermediates', action='store_true',
                        help='Keep intermediate BAM and uncompressed bedgraph files')
    parser.add_argument('--gene-names', nargs='+',
                        help='If set, filter for only these gene names; otherwise, process all')
    parser.add_argument('--gene-ids', nargs='+',
                        help='If set, filter for only these gene IDs; otherwise, process all')
    parser.add_argument('--snp-ids', nargs='+',
                        help='If set, filter for only these SNP IDs; otherwise, process all')
    parser.add_argument('--contig-override', action='store_true',
                        help='If set, map BAM contigs to VCF contigs using '
                             'the [contigs] section in the config.')
    args = parser.parse_args()
    results = np.genfromtxt(args.assocfile, delimiter='\t', names=True,
                            dtype=None, encoding=None)
    names = list(args.config.options('psifiles'))
    for entry in results[results[args.column] < args.threshold]:
        if args.gene_names is not None and entry['gene_name'] not in args.gene_names:
            continue
        gene_id = entry['lsv_id'].split(':')[0]
        if args.gene_ids is not None and gene_id not in args.gene_ids:
            continue
        snp_id = entry['snp_id']
        if args.snp_ids is not None and snp_id not in args.snp_ids:
            continue
        with pysam.TabixFile(args.gff3) as gff3:
            for gene in gff3.fetch(parser=pysam.asGFF3()):
                if gene.ID == gene_id:
                    break
            else:
                print('Could not find gene', gene_id)
                continue
        if args.contig_override and args.config.has_section('contigs'):
            contig = args.config.get('contigs', gene.contig)
        else:
            contig = gene.contig
        fetch_kw = {'contig': contig,
                    'start':  gene.start,
                    'end':    gene.end}
        with pysam.VariantFile(args.vcffile) as vcffile:
            for snp in vcffile.fetch(**fetch_kw):
                if snp.id == snp_id:
                    break
            else:
                print('Could not find snp', snp_id)
                continue
        fetch_kw['contig'] = gene.contig
        gts = [[] for ct in range(3)]
        gtstrs = '{0}_{0} {0}_{1} {1}_{1}'.format(*snp.alleles).split()
        for name, sample in snp.samples.items():
            name = gtex_convert_name(name)
            if name in names and None not in sample.allele_indices:
                gts[sum(sample.allele_indices)].append(name)
        for ct, gt in enumerate(gts):
            if len(gt) == 0:
                print('No samples for {} {}'.format(snp_id, gtstrs[ct]))
                print(names)
                print(snp.samples.keys())
                exit(0)
                break
        else:
            for ct, gt in enumerate(gts):
                name = np.random.choice(gt)
                srr = args.config.get('psifiles', name).split('.')[0]
                for bamfile in args.bams:
                    if srr in bamfile:
                        break
                else:
                    print('could not find BAM file', srr)
                    continue
                tname = '{}.{:d}.{}.{}.{}'.format(snp_id,
                                                  ct,
                                                  gtstrs[ct],
                                                  entry['lsv_id'],
                                                  name)
                ofbase = '{}.bam'.format(tname)
                ofname = os.path.join(args.outdir, ofbase)
                os.makedirs(os.path.join(args.outdir, tname), exist_ok=True)

                print('Selecting reads for', ofname)
                with pysam.AlignmentFile(bamfile, 'r') as inbam:
                    with pysam.AlignmentFile(ofname, 'wb', template=inbam) as outbam:
                        for read in inbam.fetch(**fetch_kw):
                            outbam.write(read)
                        trackopts = 'name="{}" ' \
                                    'visibility=2 ' \
                                    'db=hg19'.format(tname)

                print('Computing genome coverage for', ofname)
                bedtool = pybedtools.BedTool(ofname)
                bedgraph = bedtool.genome_coverage(bg=True, split=True,
                                                   trackline=True,
                                                   trackopts=trackopts)
                bedgraph = bedgraph.saveas(ofname.replace('.bam', '.bedgraph'))
                if not args.keep_intermediates:
                    os.remove(ofname)
                    os.rmdir(os.path.join(args.outdir, tname))
                if args.bgzip:
                    bedgraph.bgzip(force=True)
                    if not args.keep_intermediates:
                        os.remove(ofname.replace('.bam', '.bedgraph'))


if __name__ == '__main__':
    main()

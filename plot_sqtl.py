import csv
import os
import scipy.stats as st
import scipy.special as sc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as pch
import matplotlib.text as txt
import argparse
import palettable
from progressbar import ProgressBar
from sklearn.impute import SimpleImputer
from src.utils import makedirs_exist_ok, residualize, load_covars
from src.utils import apply_normalization, common, sqtl_parse
from src.voila_io import load_lsvs, Lsv
from src.vcf_io import load_snps, Snp
try:
    from voila.api.splice_graph import Exons
except ImportError:
    from voila.api.splice_graph_sql import Genes as Exons


def get_junc_color(i):
    junc_colors = palettable.colorbrewer.qualitative.Set1_9.mpl_colors
    r, g, b = junc_colors[i % len(junc_colors)]
    light_scale = i // len(junc_colors)
    if light_scale > 0:
        rdiff = 255 - r
        gdiff = 255 - g
        bdiff = 255 - b
        for i in range(light_scale):
            rdiff *= 0.4
            gdiff *= 0.4
            bdiff *= 0.4
        r = int(255 - rdiff)
        g = int(255 - gdiff)
        b = int(255 - bdiff)
    return r, g, b


def plot_current(row, lsv: Lsv, snp: Snp, covariates, args):
    ref, alt = snp.alleles
    genos = ['{}/{} (N={})'.format(*x)
             for x in ((ref, ref, row['num_homo_a']),
                       (ref, alt, row['num_hetero']),
                       (alt, alt, row['num_homo_b']))]
    if 'jn_coords' in row:
        jn_start, jn_end = map(int, row['jn_coords'].split(':')[1].split('-'))
        jns = lsv.junctions.index((jn_start, jn_end)),
    else:
        jns = int(row['jn_idx']),
    for jn_idx in jns:
        psi = lsv.psi[:, jn_idx]
        psi = SimpleImputer().fit_transform(np.reshape(psi, (-1, 1)))[:, 0]
        psi_orig = psi.copy()
        if args.logit:
            psi = sc.logit(psi)
        psi = apply_normalization(psi, args.normalize)
        if covariates is not None and args.normalize != 'off':
            covars = SimpleImputer().fit_transform(lsv.get_covars(covariates)).T
            psi = residualize(covars, psi)
        gnt = snp.gnt(lsv)
        homo_r = psi[gnt == 0]
        hetero = psi[gnt == 1]
        homo_a = psi[gnt == 2]

        plt.figure()
        nanidxs = np.where([not np.isnan(g) for g in gnt])
        jitter = st.beta.rvs(2, 2, size=nanidxs[0].size, loc=.55, scale=.9)
        boxdict = plt.boxplot([homo_r, hetero, homo_a], vert=True, showmeans=True,
                              showfliers=False)
        scatter = plt.scatter(gnt[nanidxs] + jitter, psi[nanidxs],
                              s=4, c=[get_junc_color(jn_idx)])
        plt.xticks(range(1, 4), genos)
        if args.normalize == 'off':
            normstr = ''
        elif args.normalize == 'standard':
            normstr = 'Normalized '
        else:
            normstr = 'Quantile normalized '
        ylabel = '%s%sJunction %s$\\Psi$' % (normstr,
                                    'Transformed ' if args.logit else '',
                                    'Corrected ' if args.logit and covariates is not None else '')
        plt.ylabel(ylabel)
        if 0 in gnt and 2 in gnt:
            abs_dpsi = abs(psi_orig[gnt == 0].mean() - psi_orig[gnt == 2].mean())
        else:
            abs_dpsi = np.nan
        plt.title('{}:{:d}-{:d}#{:d} vs {}\n'
                  'dist = {}, {:s} = {}\n'
                  '$E[|\\Delta\\Psi|] = {}$'
                  .format(lsv.name, lsv.start, lsv.end, jn_idx, snp.id,
                          row['distance'], args.field, row[args.field],
                          abs_dpsi))
        if not args.logit and args.normalize == 'off':
            plt.ylim(-0.05, 1.05)
        plt.tight_layout()
        ofname = os.path.join(args.outdir, '{}_{}_{}.pdf'.format(lsv.id, snp.id, jn_idx))
        plt.savefig(ofname, transparent=True)
        plt.close()


def plot_splicegraphs(args, lsv, snps):
    plt.figure(figsize=[10, 2])

    plot_min = lsv.start
    plot_max = lsv.end
    # for snp in snps:
    #     plot_min = min(snp.pos, plot_min)
    #     plot_max = max(snp.pos, plot_max)
    for start, end in lsv.exons:
        plot_min = min(plot_min, start)
        plot_max = max(plot_max, end)
    plot_len = plot_max - plot_min + 1
    plot_min -= 100
    plot_max += 100

    exon_patches = []
    exon_labels = []
    junction_patches = []
    snp_patches = []
    snp_labels = []

    if hasattr(args.plot_splicegraph, 'exons'):
        exons = [exon for exon in args.plot_splicegraph.exons(lsv.gene_id) if -1 not in (exon['start'], exon['end'])]
    else:
        exons = [exon.__dict__ for exon in args.plot_splicegraph.gene(lsv.gene_id).get.exons if -1 not in (exon.start, exon.end)]

    if lsv.strand == '-':
        exons = reversed(exons)

    for exon_idx, exon in enumerate(exons):
        start = exon['start']
        end = exon['end']
        if start == lsv.start and end == lsv.end:
            color = '#FFA500'
        elif (start, end) in lsv.exons:
            color = '#808080'
        else:
            color = '#EEEEEE'
        exon_patches.append(
            pch.Rectangle((start, -0.2), end - start, 0.4, facecolor=color, edgecolor=(0, 0, 0), zorder=4))
        exon_c = (start + end) / 2
        if plot_min < exon_c < plot_max:
            exon_labels.append(
                plt.text((start + end) / 2, 0, f'{exon_idx + 1:d}', color='k', zorder=7, horizontalalignment='center',
                         verticalalignment='center', fontsize=12))

    for jn_idx, (start, end) in enumerate(lsv.junctions):
        junction_patches.append(
            pch.Arc(((start + end) / 2, 0.2), end - start, 1.0, theta1=0, theta2=180, color=get_junc_color(jn_idx),
                    linewidth=2, zorder=3))

    for start, end in lsv.intron_retention:
        junction_patches.append(
            pch.Rectangle((start, -0.05), end - start, 0.1, facecolor=get_junc_color(lsv.njunc), edgecolor=(0, 0, 0), zorder=2))

    for snp in snps:
        if plot_min + 100 <= snp.pos <= plot_max:
            snp_patches.append(plt.plot([snp.pos, snp.pos], [-0.1, 0.1], color='k', zorder=5))
            snp_labels.append(
                plt.text(snp.pos, -0.2, snp.id, color='k', rotation=-45, zorder=6, fontsize=10,
                         horizontalalignment='left', verticalalignment='top'))

    plt.plot([plot_min, plot_max], [0, 0], color='k', zorder=1)
    plt.axis('off')

    plt.xlim(plot_min, plot_max)
    plt.ylim(-1.2, 1.2)
    plt.tight_layout(0)
    ax: plt.Axes = plt.gca()
    for patch in exon_patches + junction_patches:
        ax.add_patch(patch)
    # txts = []
    # for text in exon_labels:
    #     ax.add_artist(text)
    # for text in snp_labels:
    #     txts.append(ax.add_artist(text))
    if lsv.strand == '-':
        a, b = plt.xlim()
        plt.xlim(b, a)
    ofname = os.path.join(args.outdir, '{}_splicegraph.pdf'.format(lsv.id))
    plt.savefig(ofname, transparent=True)

    plt.close()


def main():
    parser = argparse.ArgumentParser(parents=[common, sqtl_parse])
    parser.add_argument('pvals', type=argparse.FileType(),
                        help='Path to call_sqtls.py output')
    parser.add_argument('outdir', type=makedirs_exist_ok,
                        help='Directory to which output is to be saved')
    parser.add_argument('--field', default='ftest_pval',
                        help='Field by which to filter on')
    parser.add_argument('--cutoff', type=float, default=0.05,
                        help='P-value or FDR threshold')
    parser.add_argument('--plot-splicegraph', type=Exons,
                        help='Path to MAJIQ splicegraph.sql.  If not specified, will not plot the splicegraph mockups '
                             '(*.splicegraph.pdf).')
    parser.add_argument('--filter-gene-names', action='append',
                        help='Space-separated list of gene names to include in the analysis.  If not specified, will '
                             'analyze all genes.')
    parser.add_argument('--filter-snp-ids', action='append',
                        help='Space-separated list of SNP IDs to include in the analysis.  If not specified, will '
                             'anaylze all SNPs.')
    parser.add_argument('--splicegraph-only', dest='do_scatter', action='store_false',
                        help='If set, will skip plotting the scatterboxes.')
    args = parser.parse_args()

    stats = []
    lsv_ids = set()
    snp_ids = set()
    reader = csv.DictReader(args.pvals, dialect='excel-tab')
    for row in reader:
        if float(row[args.field]) >= args.cutoff:
            continue
        if args.filter_gene_names and row['gene_name'] not in args.filter_gene_names:
            continue
        if args.filter_snp_ids and row['snp_id'] not in args.filter_snp_ids:
            continue
        stats.append(row)
        lsv_ids.add(row['lsv_id'])
        snp_ids.add(row['snp_id'])

    lsvs = load_lsvs(args, id_filter=lsv_ids)
    snps = load_snps(args, id_filter=snp_ids)
    covariates = load_covars(args) if args.covariates else None

    if args.do_scatter:
        print('Making scatterplots')
        bar = ProgressBar(max_value=len(stats))
        for row in bar(stats):
            if row['lsv_id'] in lsvs and row['snp_id'] in snps:
                plot_current(row, lsvs[row['lsv_id']], snps[row['snp_id']],
                             covariates, args)
            else:
                print(lsvs)
                raise KeyError(row['lsv_id'])

    if args.plot_splicegraph is not None:
        print('Drawing LSV splicegraphs')
        bar = ProgressBar(max_value=len(lsv_ids))
        for lsv_id in bar(lsv_ids):
            plot_splicegraphs(args, lsvs[lsv_id], [snps[row['snp_id']] for row in stats if row['lsv_id'] == lsv_id])


if __name__ == '__main__':
    main()

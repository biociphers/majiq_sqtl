#!/bin/sh -xe

# Paths listed here are for the bone growth project (Penn/CHOP lead: Struan Grant, PhD).
# They refer to files on pickle1.

# Artifacts in the current working directory:
# settings.ini: Preprocessed config for MAJIQ builder
# sqtl.ini: Preprocessed config for sQTL calling
# sqtl.tsv: TSV output from sQTL calling
# sqtl_plots/: Scatterbox and splicegraph plots directory
# sqtl_bedgraphs/: bedGraph.gz files (essentially bigwig) covering the gene locus, delimited by SNP genotype as indicated


## DEFINE VARIABLES
# Path to a python3.6 virtual environment with MAJIQ 2.0 installed
VENV_ROOT=/opt/venv/py3
# Path to the MAJIQ_SQTL clone directory
SRC=${HOME}/majiq_sqtl
# Directory with the BAM files
BAM=/data/gtex/adrenal/bam
# Path to the GFF3 annotation
GFF3=/data/projects/struan_cyp11b1/cyp11b1.gff3
# Path to experiment VCF file
VCF=/data/gtex/genotype/gtex_v6p_cyp11b1.vcf.gz
# Path to covariates file
COVARS=/data/gtex/covariates/Adrenal_Gland.v7.covariates.txt
# Path to VCF file mapping dbSNP names (GTEx uses generic tags)
DBSNP=/data/gtex/genotype/GRCh37_latest_dbSNP_all.vcf.gz

## RUN ALL
. ${VENV_ROOT}/bin/activate
# Ensure the environment is up to date
pip install -U pip
pip install -U -r ${SRC}/requirements.txt
# Builder
sed 's/##BAMDIR##/'${BAM}'/' settings.ini.in > settings.ini
majiq build ${GFF3} -o build -c settings.ini -j 16
# Psi
for majiqFile in build/*.majiq; do
    majiq psi ${majiqFile} -o psi -j 16
done
# Voila
for voilaFile in psi/*.voila; do
    voila tsv -f ${voilaFile%.*}.tsv ${voilaFile} build
done
# sQTL
sed 's/##PSIDIR##/'$(realpath ./psi)'/' sqtl.ini.in > sqtl.ini
python3 ${SRC}/call_sqtls.py sqtl.ini sqtl.tsv -v ${VCF} -c ${COVARS} -s linreg ftest --dbsnp ${DBSNP} --logit --normalize quantile -r 10000 --extend-exons --contig-override --show-all
# Plotting
python3 ${SRC}/plot_sqtl.py sqtl.ini sqtl.tsv ./sqtl_plots/ -v ${VCF} --field linreg_fdr --dbsnp ${DBSNP} --normalize off --plot-splicegraph build/splicegraph.sql
# Bedgraphs
python3 ${SRC}/get_sample_beds.py ${VCF} sqtl.tsv ${GFF3} ./sqtl_bedgraphs sqtl.ini ${BAM} --bgzip --column linreg_fdr

# Cleanup
deactivate

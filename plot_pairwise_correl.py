import argparse
import csv
import matplotlib.pyplot as plt
import scipy.stats as st
from collections import defaultdict


def get_signif_assocs(fp, args):
    reader = csv.DictReader(fp, dialect='excel-tab')
    yield reader.fieldnames
    yield from filter(lambda line: float(line[args.key]) < args.alpha, reader)


def plot_correl(args):
    lsvs = defaultdict(dict)
    for name, fp in zip(args.names, args.infiles):
        reader = csv.DictReader(fp, dialect='excel-tab')
        for line in reader:
            lsvs[(line['lsv_id'], line['snp_id'])][name] = float(line[args.key])

    for i, name1 in enumerate(args.names[:-1]):
        for name2 in args.names[i+1:]:
            print(name1, name2)
            v1 = []
            v2 = []
            for assoc, vals in lsvs.items():
                if name1 in vals and name2 in vals:
                    v1.append(vals[name1])
                    v2.append(vals[name2])
            if v1:
                plt.figure(figsize=[10, 10])
                plt.scatter(v1, v2)
                slope, intercept, rvalue, pvalue, stderr = st.linregress(v1, v2)
                plt.plot([0, 1], [intercept, slope + intercept], c='k')
                plt.xlim(0, args.alpha)
                plt.ylim(0, args.alpha)
                plt.title(f'{name1} vs {name2} ($R = {rvalue}$)')
                plt.savefig(f'{args.outfile}_{name1}_{name2}', transparent=True)
                plt.close()
            else:
                print('Nothing found')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('outfile')
    parser.add_argument('infiles', nargs='+', type=argparse.FileType())
    parser.add_argument('--alpha', type=float, default=0.05)
    parser.add_argument('--key', default='linreg_fdr')
    parser.add_argument('--names', nargs='+', required=True)
    args = parser.parse_args()
    assert len(args.names) == len(args.infiles)
    plot_correl(args)


if __name__ == '__main__':
    main()

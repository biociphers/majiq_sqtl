import os
import subprocess


try:
    COMMIT_RAW = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD'],
                                         cwd=os.path.dirname(__file__))
except subprocess.CalledProcessError:
    COMMIT_RAW = b'staticdist'
COMMIT = COMMIT_RAW.decode().strip()
VERSION = '0.2.0-het-' + COMMIT
PKGNAME = 'MAJIQ-L'
PKGSTR = PKGNAME + ' ' + VERSION

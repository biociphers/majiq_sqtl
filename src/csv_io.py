import csv
import collections
from src.stats import statistics

TEMPLATE = collections.OrderedDict()
TEMPLATE['chromosome'] = ''
TEMPLATE['ref_start'] = 0
TEMPLATE['ref_end'] = 0
TEMPLATE['lsv_id'] = ''
TEMPLATE['gene_name'] = ''
TEMPLATE['jn_coords'] = ''
TEMPLATE['is_ir'] = False
TEMPLATE['snp_id'] = ''
TEMPLATE['snp_pos'] = 0
TEMPLATE['snp_alleles'] = './.'
TEMPLATE['distance'] = 0
TEMPLATE['num_homo_a'] = 0
TEMPLATE['epsi_homo_a'] = 0.
TEMPLATE['num_hetero'] = 0
TEMPLATE['epsi_hetero'] = 0.
TEMPLATE['num_homo_b'] = 0
TEMPLATE['epsi_homo_b'] = 0.


def create_dict(args, lsv, snp, jn_idx, summary, values):
    summary_dict = TEMPLATE.copy()
    jnstart, jnend = lsv.junctions[jn_idx]
    summary_dict['chromosome'] = lsv.chrom
    summary_dict['ref_start'] = lsv.start
    summary_dict['ref_end'] = lsv.end
    summary_dict['lsv_id'] = lsv.id
    summary_dict['gene_name'] = lsv.name
    summary_dict['jn_coords'] = f'{lsv.chrom}:{jnstart}-{jnend}'
    summary_dict['is_ir'] = len(lsv.intron_retention) != 0
    summary_dict['snp_id'] = snp.id
    summary_dict['snp_pos'] = snp.pos
    summary_dict['snp_alleles'] = f'{snp.major_allele}/{snp.minor_allele}'
    summary_dict['distance'] = summary[0]
    summary_dict['num_homo_a'] = summary[1]
    summary_dict['epsi_homo_a'] = summary[2]
    summary_dict['num_hetero'] = summary[3]
    summary_dict['epsi_hetero'] = summary[4]
    summary_dict['num_homo_b'] = summary[5]
    summary_dict['epsi_homo_b'] = summary[6]
    # Record the output stats
    for stat, statvec in zip(args.stats, values):
        offset = 0
        for value in statvec:
            # Skip fdr
            while stat.outnames[offset].endswith('fdr'):
                summary_dict[stat.outnames[offset]] = 1.0
                offset += 1
            summary_dict[stat.outnames[offset]] = value
            offset += 1
    return summary_dict


def get_fieldnames(args):
    fieldnames = list(TEMPLATE.keys())
    for stat in args.stats:
        fieldnames += statistics[stat].outnames
    return fieldnames


def csv_write(args, data):
    """Helper function to write sQTL calls to a TSV file"""
    fieldnames = get_fieldnames(args)
    for row in data:
        for key, value in row.items():
            if isinstance(value, float):
                row[key] = f'{value:6g}'
            else:
                row[key] = str(value)
    writer = csv.DictWriter(args.output, fieldnames, dialect='excel-tab')
    writer.writeheader()
    writer.writerows(data)

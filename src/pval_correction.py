import numpy as np
import statsmodels.stats.multitest as mt


def correct_pvalues(outstats, args):
    """Performs FDR correction.

    Input:
        outstats is a numpy.ndarray with named fields that contains the
            output of the association model
        args is the Namespace object generated by the argument parser:
            args.alpha: The desired type I error rate for FDR correction
            args.two_stage: If set, performs a two-stage correction that
                involves a local correction within genes followed by
                a BH correction between genes
            args.npermute: Number of rounds of permutation testing.
                If 0 (default), performs a single-pass global BH correction
                over the most significant association per gene, with the
                correction fit to the other associations by a degree 3
                polynomial fit in log space.
                Otherwise, attempts to estimate FDR by local permutation of
                p-values per gene, followed by a BH correction between genes.
                Only effectual if args.two_stage is true.

    Output:
        The same as the input, but with the FDR columns filled in for each
            stat, and sorted by increasing p-value in the lowest-indexed test
            statistic requested by the user
    """
    if hasattr(args, 'by_which'):
        key = 'gene_name' if args.by_which == 'gene' else 'lsv_id'
    else:
        key = 'gene_name'
    for stat in args.stats:
        for fdrname in stat.outnames:
            if not fdrname.endswith('_fdr'):
                continue
            pvalname = fdrname[:-4] + '_pval'
            all_pvals = [float(row[pvalname]) for row in outstats]
            if args.two_stage:
                seen_genes = {}
                for pval, row in zip(all_pvals, outstats):
                    seen_genes[row[key]] = min(seen_genes.get(row[key], 1.0), pval)
                pvalues = sorted(seen_genes.values())
                rejected, corrected, _, _ = mt.multipletests(pvalues, args.alpha, method='fdr_bh', is_sorted=True)
                all_corrected = np.interp(all_pvals, pvalues, corrected, left=0, right=1)
                for fdr, row in zip(all_corrected, outstats):
                    row[fdrname] = fdr
            else:
                rejected, corrected, _, alpha_bonf = mt.multipletests(all_pvals, args.alpha, method='bonferroni',
                                                                      is_sorted=False)
                for row, cpval in zip(outstats, corrected):
                    row[fdrname] = cpval
    return outstats

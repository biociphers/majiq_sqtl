import scipy.stats as st
from src.stats import Statistic


class Ftest(Statistic):
    name = 'ftest'
    dirn = False
    outnames = ['ftest_stat', 'ftest_pval', 'ftest_fdr']
    offsets = [2]
    null = [0, 1]

    def __call__(self, labels, values, **kwargs):
        return self.discrete(st.f_oneway, *self.get_groups(labels, values))

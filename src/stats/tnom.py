import numpy as np
import scipy.special as sc
import scipy.misc as sm
from src.stats import Statistic


__logpmf_cache__ = {}
__pval_cache__ = {}


def integers():
    idx = 0
    while True:
        yield idx
        idx += 1


def combiln(n, k):
    return sc.gammaln(n + 1) - sc.gammaln(k + 1) - sc.gammaln(n - k + 1)


def lpathn(l, o):
    return combiln(l, (l + abs(o)) // 2)


def mcombiln(k):
    k = np.array(k)
    return sc.gammaln(k.sum() + 1) - sc.gammaln(k + 1).sum()


def logpmf(counts, score, normalize=True):
    c1, c2, c3 = counts
    ntot = sum(counts)
    key = tuple(sorted(counts)) + (score,)
    logp = __logpmf_cache__.get(key)
    if logp is None:
        clscore = combiln(ntot, score)
        logp = []
        for s1 in range(min(c1, score) + 1):
            cl1 = combiln(c1, s1) + clscore
            for s2 in range(min(c2, score - s1) + 1):
                s3 = score - s1 - s2
                if s3 > c3:
                    continue
                logp.append(cl1 + combiln(c2, s2) + combiln(c3, s3))
        logp = sc.logsumexp(logp)
        logp -= mcombiln(counts)
        if normalize:
            logp -= sc.logsumexp([logpmf(counts, ss, normalize=False)
                                  for ss in range(ntot + 1)])
        __logpmf_cache__[key] = logp
    return logp


def logcdf(counts, score):
    logp = sc.logsumexp([logpmf(counts, ss) for ss in range(score + 1)])
    if logp > 0.0:
        logp = 0.0
    return logp


def pmf(counts, score):
    return np.exp(logpmf(counts, score))


def cdf(counts, score):
    """
    Gets TNOM p value for k classes of sizes counts with given score from the
    TNOM test.
    """
    return np.exp(logcdf(counts, score))


def pvalue(neg, pos, score):
    """
    Arguments: neg, pos, score
    Returns p-value for TNOM score given neg negative and pos positive labels.
    """
    pval = __pval_cache__.get((neg, pos, score)) or \
           __pval_cache__.get((pos, neg, score))
    if pval is None:
        if (pos <= score) or (neg <= score) or 0 in (neg, pos):
            pval = 1.
        else:
            l = pos + neg
            o = pos - neg
            offs = np.array([l - 2 * neg, l - 2 * pos])
            nums = -np.inf * np.ones(2)
            delta = 2 * (np.array([pos, neg]) - score)
            cts = np.zeros(2)
            for sn in integers():
                cts += delta
                cns = abs(cts - offs)
                if (cns > l).any():
                    break
                chg = sm.logsumexp([lpathn(l, cn) for cn in cns])
                nums[sn % 2] = np.logaddexp(nums[sn % 2], chg)
                cts = np.roll(cts, 1)
            npath = lpathn(l, o)
            diffs = np.exp(nums - npath)
            pval = abs(np.diff(diffs))[0]
            pval = np.clip(pval, 0., 1.)
        __pval_cache__[(neg, pos, score)] = pval
        __pval_cache__[(pos, neg, score)] = pval
    return pval


def tnom_2way(data, truth, assume_sorted=False, return_threshold=False):
    """TNOM operator

    :param data: numpy array of values (may be sorted)
    :param truth: numpy array of boolean labels
    :param assume_sorted: if true, don't sort data
    :param return_threshold: if true, also return the decision boundary
    :return: p value and score for TNOM
    """
    truth = np.array(truth)
    data = np.array(data)
    if not assume_sorted:
        asorted = np.argsort(data, kind='mergesort')
        truth = truth[asorted]
        data = data[asorted]
    n = data.size
    if n == 0:
        if return_threshold:
            return 1.0, 0, np.nan
        return 1.0, 0
    pos = truth.sum()
    neg = n - pos
    if 0 in (neg, pos):
        pval = 1.
        best_loss = 0
        threshold = data[0]
    else:
        cutoffs = np.append([data[0] - 1], np.append(data, [data[-1] + 1]))
        cutoffs = cutoffs[:-1] + 0.5 * np.diff(cutoffs)
        cutoffs = np.setdiff1d(cutoffs, data)
        preds = np.less.outer(data, cutoffs) ^ truth[0]
        losses = (preds == truth.reshape((-1, 1))).sum(0)
        best_idx = losses.argmin()
        best_loss = losses[best_idx]
        threshold = cutoffs[best_idx]
        pval = pvalue(neg, pos, best_loss)
    if return_threshold:
        return pval, best_loss, threshold
    return pval, best_loss


def get_score(data, truth, cutoffs, n, u_truth):
    lefts = np.less.outer(data, cutoffs)
    rights = 1 - lefts
    labels = 1 - np.equal.outer(truth, u_truth)
    best_loss = n
    best_tau1 = cutoffs[0] - 1
    best_tau2 = cutoffs[-1] + 1
    for i in range(cutoffs.size - 1):
        grp1 = lefts[:, i]
        l1 = grp1.dot(labels)
        for j in range(i + 1, cutoffs.size):
            grp2 = rights[:, i] * lefts[:, j]
            grp3 = rights[:, j]
            l2 = grp2.dot(labels)
            l3 = grp3.dot(labels)
            cur_loss = min([l1[0] + l2[1] + l3[2],
                            l1[0] + l2[2] + l3[1],
                            l1[1] + l2[0] + l3[2],
                            l1[1] + l2[2] + l3[0],
                            l1[2] + l2[0] + l3[1],
                            l1[2] + l2[1] + l3[0]])
            if cur_loss < best_loss:
                best_loss = cur_loss
                best_tau1 = cutoffs[i]
                best_tau2 = cutoffs[j]
    return best_loss, best_tau1, best_tau2


def tnom_3way(data, truth, assume_sorted=False, return_threshold=False):
    truth = np.array(truth)
    data = np.array(data)
    if not assume_sorted:
        asorted = np.argsort(data)
        truth = truth[asorted]
        data = data[asorted]
    u_truth = np.unique(truth)
    if u_truth.size < 3:
        if return_threshold:
            pval, best_loss, tau1 = tnom_2way(data, truth, assume_sorted=True,
                                              return_threshold=True)
            return pval, best_loss, tau1, data[-1] + 1
        return tnom_2way(data, truth, assume_sorted=True)
    bin_features = np.equal.outer(u_truth, truth)
    counts = bin_features.sum(axis=1)
    n = counts.sum()
    cutoffs = np.append([data[0] - 1], np.append(data, [data[-1] + 1]))
    cutoffs = cutoffs[:-1] + 0.5 * np.diff(cutoffs)
    cutoffs = np.setdiff1d(cutoffs, data)
    best_loss, best_tau1, best_tau2 = get_score(data, truth, cutoffs, n,
                                                u_truth)
    pval = cdf(counts, best_loss)
    if return_threshold:
        return pval, best_loss, best_tau1, best_tau2
    return pval, best_loss


class Tnom(Statistic):
    name = 'tnom'
    dirn = True
    outnames = ['tau1', 'tau2', 'tnom_stat', 'tnom_pval', 'tnom_fdr']
    offsets = [4]
    null = [0, 1, np.inf, 1]

    def __call__(self, labels, values, **kwargs):
        idxs = np.where(np.isnan(labels) ^ True)
        pval, tnom_, tau1, tau2 = tnom_3way(values[idxs], labels[idxs],
                                            return_threshold=True)
        return [tau1, tau2, tnom_, pval]

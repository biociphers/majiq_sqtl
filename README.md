# MAJIQtl: Splicing QTL analysis using MAJIQ PSI estimates

This repository requires Python 3.6 or higher.  Any shell command reference
to `python` assumes that `python` is an alias for the appropriate version of
Python.  If that is not the case, you will need to specify which Python
executable to use as the interpreter e.g. `python3`, `python3.6`, or an 
explicit path to the Python binary.

## Setting up

Follow the instructions to install MAJIQ (version 2.0).

Then run the following command:

    python -m pip install -r requirements.txt

The main script is `call_sqtls.py`.  It requires a config file which must 
include at least these two containers: `info` and `psifiles`.  A third optional
container, `contigs`, may be supplied.  These sections will be explained later
in this document.

## Calling MAJIQtl from the command line

        usage: python call_sqtls.py [-h] [--pre-lsv PRE_LSV] [--refresh-db]
                                     [--log LOG] [--debug] -v VCF [-c COVARIATES]
                                     [--logit]
                                     [--normalize {off,standard,quantile}]
                                     [--dbsnp DBSNP]
                                     [-s {linreg,ttest,ftest,tnom,kruskal} [{linreg,ttest,ftest,tnom,kruskal} ...]]
                                     [-j NTHREADS] [-r SNP_RANGE] [-a ALPHA]
                                     [-p NPERMUTE] [-g MIN_GAP] [-k GAP_SELECT_K]
                                     [--extend-exons] [--two-stage]
                                     [--enable-gap-filtering] [--gwas GWAS]
                                     [--min-ld MIN_LD] [--contig-override]
                                     [--jn-sel-strategy {maxdpsi,maxtss,selall}]
                                     [--min-mac MIN_MAC] [--min-maf MIN_MAF]
                                     [--show-all]
                                     config output
    
    positional arguments:
      config                Configuration pointing to the PSI files and other
                            arguments
      output                Path to output file
    
    optional arguments:
      -h, --help            show this help message and exit
      --pre-lsv PRE_LSV     Set to a file path where preformatted LSVs are to be
                            stored. Creates this file if it does not exist.
      --refresh-db          If set, will force preprocessing of the LSVs from
                            scratch as though the file passed to --pre-lsv does
                            not exist.
      --log LOG             File to which the logger output should be written
      --debug               Extra debug log.infos
      -v VCF, --vcf VCF     Path to VCF or BCF file containing sample genotypes.
      -c COVARIATES, --covariates COVARIATES
                            Path to tab-delimited covariates file. Each column
                            represents a subject, and each row represents a
                            quantitative covariate.
      --logit               If set, will logit transform PSI.
      --normalize {off,standard,quantile}
                            Normalization strategy. Choices: off, standard,
                            quantile. Default: off
      --dbsnp DBSNP         Reference VCF for dbSNP IDs
      -s {linreg,ttest,ftest,tnom,kruskal} [{linreg,ttest,ftest,tnom,kruskal} ...], --stats {linreg,ttest,ftest,tnom,kruskal} [{linreg,ttest,ftest,tnom,kruskal} ...]
                            Which test statistics to use. Choices: linreg, ttest,
                            ftest, tnom, kruskal. Default: ['linreg', 'ttest',
                            'ftest', 'tnom', 'kruskal'].
      -j NTHREADS, --nthreads NTHREADS
                            Number of parallel workers. Default: 1
      -r SNP_RANGE, --snp-range SNP_RANGE
                            Maximum allowable distance, in BP, between an exon and
                            a putative cis-sQTL. Default: 0
      -a ALPHA, --alpha ALPHA
                            Desired Type I error rate for FDR correction. Default:
                            0.05
      -p NPERMUTE, --npermute NPERMUTE
                            Number of permutations for permutation testing.
                            Default: 0
      -g MIN_GAP, --min-gap MIN_GAP
                            Minimum difference between two adjacent PSI values
                            required for consideration of each LSV. Default: 0.05
      -k GAP_SELECT_K, --gap-select-k GAP_SELECT_K
                            Number of extreme PSI values to exclude when
                            performing PSI gap selection. Default: 3
      --extend-exons        If set, will consider putative cis-sQTLs within range
                            of any exon associated with the LSV, not just the
                            reference exon.
      --two-stage           If set, correct and filter for best association per
                            gene, then perform a second-pass correction between
                            genes. Default: perform a single pass FDR correction
                            over all LSV-SNP pairs.
      --enable-gap-filtering
                            If set, filters the candidate set of LSVs based on
                            satisfying a minimum difference in adjacent PSI
                            values.
      --gwas GWAS           Path to GWAS linkage disequilibrium file.
      --min-ld MIN_LD       Minimum R2 correlation between SNPs tested for linkage
                            disequilibrium in --gwas. SNP pairs with worse
                            correlation will be discarded.
      --contig-override     If set, will use the [contigs] section in the config
                            to map PSI chromosome IDs to VCF chromosome IDs.
      --jn-sel-strategy {maxdpsi,maxtss,selall}
                            Select the junction selection strategy. maxdpsi:
                            Select the junction with the greatest expected delta
                            PSI between homozygotes. maxtss: Select the junction
                            with the greatest total variance (total sum of
                            squares). selall: Return every junction without
                            selection (default)
      --min-mac MIN_MAC     Set the minimum minor allele count for selecting a
                            variant to test. Default: 5
      --min-maf MIN_MAF     Set the minimum minor allele frequency for selecting a
                            variant to test. Default: 0.1
      --show-all            Do not filter only significant events


## Config file

### `info`

The following argument is required in this section:

`psidir` = path to where you can find all the input psi.voila files generated 
by MAJIQ.

### `psifiles`

Each line in this section represents the Voila output from one RNA-seq
experiment.  The label shall be the deidentified subject ID.  All paths are 
relative to the directory declared in `info/psidir`.

Example:

    GTEX-12345 = SRR133742069.psi.tsv

### `contigs`

This section is meant to support the case where the chromosome identifiers in
the VCF do not exactly match the chromosome identifiers in the MAJIQ output.
Each key is a chromosome name as it appears in the MAJIQ annotation, and the
values are the corresponding names as they appear in the VCF.

Example:

    chr1 = 1

## Output

The output file is a tab-delimited text file with columns determined
dynamically from which statistics are specified in the config file.

### Constitutive columns

The first three columns comply with the [BED format](https://genome.ucsc.edu/FAQ/FAQformat.html#format1) standard.

**lsv_id** - The ID of the LSV from MAJIQ.  This ID encodes the unique gene ID,
the coordinates of the reference exon, and whether the LSV is single-source or
single-target.

**gene_name** - The recorded name of the gene locus.

**jn_coords** - The coordinates of the splice junction.

**is_ir** - Boolean, True if the junction is an intron retention event.

**snp_id** - The ID of the SNP from the VCF.

**snp_pos** - Genomic coordinate of the SNP.  The SNP is guaranteed to be in
*cis* with the gene i.e. on the same chromosome.

**snp_alleles** - Allelic variants at this position.  
Format: reference/alternative, as recorded in the VCF.

**distance** - The distance between the SNP and the splice siteprefer

**num_homo_aa** - The number of samples with genotype A/A for which the LSV was
quantifiable.

**epsi_homo_aa** - The expected PSI for genotype A/A.

**num_homo_ab** - The number of samples with genotype A/B for which the LSV was
quantifiable.

**epsi_homo_ab** - The expected PSI for genotype A/B.

**num_homo_bb** - The number of samples with genotype B/B for which the LSV was
quantifiable.

**epsi_homo_bb** - The expected PSI for genotype B/B.

### If `ftest`

**ftest_stat**, **ftest_pval**, and **ftest_fdr** are self-explanatory.

### If `ttest`

**ttest_aa_ab_stat**, **ttest_aa_ab_pval**, **test_aa_ab_fdr** are the T
statistic, pvalue, and corrected pvalue for the comparison between the A/A
and A/B genotype classes.  Similarly, **ttest_aa_bb_stat** etc. are for the
comparison between A/A and B/B, and **ttest_ab_bb_stat** etc. are for the
comparison between A/B and B/B.

### If `correl`

**slope** is the effect size of SNP genotype on PSI after logit transformation,
normalization, and correction for covariates.  **stderr** is the estimated
standard error of the effect size.  **linreg_stat** is the Pearson
correlation statistic learned from the data, and **linreg_pval** and
**linreg_fdr** are the raw and corrected pvalue for this correlation.

### If `tnom`

TNOM attempts to split PSI values into three non-overlapping groups using two
thresholds such that when you assign genotype classes to each group, the total
number of mistakes (TNOM) resulting from this assignment is minimized.  This 
test is similar to a decision tree optimizing for training error.  
**tnom_tau1** and **tnom_tau2** are the two thresholds learned from the data 
that best split the PSI values with respect to the TNOM score (returned as 
**tnom_stat**). **tnom_pval** and **tnom_fdr** are the raw and corrected pvalue 
for the TNOM score.

**Developer's warning**: Take TNOM pvalue calculations with a grain of salt. 
They should not be used in downstream analysis at this time.

### If `kruskal`

**kruskal_stat**, **kruskal_pval**, and **kruskal_fdr** are self-explanatory.

import argparse
import csv
from src.stats import statistics
from src.pval_correction import correct_pvalues


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile')
    parser.add_argument('-s', '--stats', nargs='+',
                        type=lambda s: statistics[s],
                        choices=list(statistics.keys()),
                        default=list(statistics.values()),
                        help='Which test statistics to use. '
                             'Choices: %(choices)s. '
                             'Default: %(default)s.')
    parser.add_argument('-a', '--alpha', type=float, default=0.05,
                        help='Desired Type I error rate for FDR correction. '
                             'Default: %(default).3g')
    parser.add_argument('-p', '--npermute', type=int, default=0,
                        help='Number of permutations for permutation testing. '
                             'Default: %(default)d')
    parser.add_argument('--two-stage', action='store_true',
                        help='If set, correct and filter for best association '
                             'per gene, then perform a second-pass correction '
                             'between genes. Default: perform a single pass '
                             'FDR correction over all LSV-SNP pairs.')
    parser.add_argument('--by-which', choices=['gene', 'lsv'], default='gene',
                        help='When doing two-stage correction, this sets '
                             'whether the correction is on the best '
                             'association per gene or the best association '
                             'per LSV. '
                             'Choices: %(choices)s. '
                             'Default: %(default)s')

    args = parser.parse_args()
    with open(args.infile) as fp:
        reader = csv.DictReader(fp, dialect='excel-tab')
        data = list(reader)
    correct_pvalues(data, args)
    with open(args.infile, 'w') as fp:
        writer = csv.DictWriter(fp, reader.fieldnames, dialect='excel-tab')
        writer.writeheader()
        writer.writerows(data)


if __name__ == '__main__':
    main()
